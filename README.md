# sumLoc

Sums up location based data from Google Takeout

**Usage**: php sumLoc.php <Path/to/JSON-File(s)> [List of placeIds]
