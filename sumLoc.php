<?php
/**
 * sumLoc.php
 *
 * @author    Rolf Strathewerd <rolf@scaldra.net>
 * @copyright 2022 Rolf Strathewerd. All rights reserved.
 * @license   https://www.gnu.org/licenses/gpl-3.0.txt
 */

if (empty($argv[1])) {
    die("Sums up location based data\nUsage: php sumLoc.php <Path/to/JSON-File(s)> [List of placeIds]\n");
}
if (!file_exists($argv[1])) {
    die("Error: file/directory does not exists");
}

$arrFiles = array();
if (is_dir($argv[1])) {
    $path   = $argv[1][strlen($argv[1]) - 1] === '/' ? $argv[1] : $argv[1] . '/';
    $handle = opendir($path);
    if ($handle) {
        while (($entry = readdir($handle)) !== false) {
            if ($entry != '.' && $entry != '..') {
                $arrFiles[] = $path . $entry;
            }
        }
    }
    closedir($handle);
} else {
    $arrFiles[] = $argv[1];
}

$placeIds = [];
if (isset($argv[2])) {
    $placeIds = explode(",", $argv[2]);
}

$parser = new Parser();
$parser->parse($arrFiles, $placeIds);
$parser->dump();


class Parser
{
    private $places = [];
    private $metaPlace;

    public function __construct()
    {
        $this->metaPlace = new Place();
    }

    public function parse($arrFiles, $placeIds)
    {
        foreach ($arrFiles as $arrFile) {
            $json = json_decode(file_get_contents($arrFile), false);
            foreach ($json->timelineObjects as $timeObject) {
                if (isset($timeObject->placeVisit)) {
                    $place = null;
                    if (isset($this->places[$timeObject->placeVisit->location->placeId])) {
                        $place = $this->places[$timeObject->placeVisit->location->placeId];
                    } else {
                        $place = new Place();
                        $place->setPlaceId($timeObject->placeVisit->location->placeId);
                        $place->setLatitudeE7($timeObject->placeVisit->location->latitudeE7);
                        $place->setLongitudeE7($timeObject->placeVisit->location->longitudeE7);
                        if (isset($timeObject->placeVisit->location->name)) {
                            $place->setName($timeObject->placeVisit->location->name);
                        }
                        if (isset($timeObject->placeVisit->location->address)) {
                            $place->setAddress($timeObject->placeVisit->location->address);
                        }
                        if (isset($timeObject->placeVisit->location->semanticType)) {
                            $place->setSemanticType($timeObject->placeVisit->location->semanticType);
                        }

                    }

                    $dtStart  = DateTime::createFromFormat('Y-m-d\TH:i:s+',
                        $timeObject->placeVisit->duration->startTimestamp);
                    $dtEnd    = DateTime::createFromFormat('Y-m-d\TH:i:s+',
                        $timeObject->placeVisit->duration->endTimestamp);
                    $dayStart = (int)$dtStart->format('z');
                    $dayEnd   = (int)$dtEnd->format('z');

                    $interval = $dtEnd->diff($dtStart);
                    $place->setTime($place->getTime() + $interval->i + $interval->h * 60 + $interval->d * 60 * 24);

                    for ($day = $dayStart; $day <= $dayEnd; $day++) {
                        $place->setDay($day);
                        if (in_array($place->getPlaceId(), $placeIds)) {
                            $this->metaPlace->setDay($day);
                        }
                    }

                    $this->places[$timeObject->placeVisit->location->placeId] = $place;
                }
            }
        }
    }

    public function dump()
    {
        uasort($this->places, 'Place::cmp');
        echo "Address\t" .
            "Name\t" .
            "PlaceId\t" .
            "LatitudeE7\t" .
            "LongitudeE7\t" .
            "SemanticType\t" .
            "Minutes\t" .
            "Days\n";
        foreach ($this->places as $place) {
            echo $place->getAddress() . "\t" .
                $place->getName() . "\t" .
                $place->getPlaceId() . "\t" .
                $place->getLatitudeE7() . "\t" .
                $place->getLongitudeE7() . "\t" .
                $place->getSemanticType() . "\t" .
                $place->getTime() . "\t" .
                $place->getDays() . "\n";

        }
        if ($this->metaPlace->getDays() > 0) {
            echo "\nSum placeIds: " . $this->metaPlace->getDays();
        }
    }
}

class Place
{
    /**
     * @var string
     */
    private $placeId;
    /**
     * @var string
     */
    private $latitudeE7;
    /**
     * @var string
     */
    private $longitudeE7;
    /**
     * @var string|null
     */
    private $address;
    /**
     * @var string|null
     */
    private $name;
    /**
     * @var string|null
     */
    private $semanticType;
    /**
     * @var int
     */
    private $time;

    private $days = [];

    /**
     * @return string
     */
    public function getPlaceId()
    {
        return $this->placeId;
    }

    /**
     * @param string $placeId
     * @return Place
     */
    public function setPlaceId($placeId)
    {
        $this->placeId = $placeId;
        return $this;
    }

    /**
     * @return string
     */
    public function getLatitudeE7()
    {
        return $this->latitudeE7;
    }

    /**
     * @param string $latitudeE7
     * @return Place
     */
    public function setLatitudeE7($latitudeE7)
    {
        $this->latitudeE7 = $latitudeE7;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongitudeE7()
    {
        return $this->longitudeE7;
    }

    /**
     * @param string $longitudeE7
     * @return Place
     */
    public function setLongitudeE7($longitudeE7)
    {
        $this->longitudeE7 = $longitudeE7;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return Place
     */
    public function setAddress($address)
    {
        $address       = $this->cleanString(str_replace(", ", "\n", $address));
        $this->address = implode(", ", array_reverse(explode("\n", $address)));
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Place
     */
    public function setName($name)
    {
        $this->name = $this->cleanString($name);
        return $this;
    }

    /**
     * @return int
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return string
     */
    public function getFormattedTime()
    {
        $x       = $this->time;
        $minutes = $x % 60;
        $x       = ($x - $minutes) / 60;
        $hours   = $x % 24;
        $days    = ($x - $hours) / 24;
        return "$days $hours $minutes";
    }

    /**
     * @param int $time
     * @return Place
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * @return int
     */
    public function getDays()
    {
        return count($this->days);
    }

    /**
     * @param int $day
     * @return Place
     */
    public function setDay($day)
    {
        $this->days[$day] = true;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSemanticType()
    {
        return $this->semanticType;
    }

    /**
     * @param string|null $semanticType
     * @return Place
     */
    public function setSemanticType($semanticType)
    {
        $this->semanticType = $semanticType;
        return $this;
    }

    static function cmp(Place $a, Place $b)
    {
        if ($a->getAddress() == $b->getAddress()) {
            return 0;
        }
        return ($a->getAddress() < $b->getAddress()) ? -1 : 1;
    }

    /**
     * @param string|null $semanticType
     * @return string|null
     */
    private function cleanString($string)
    {
        if (empty($string)) {
            return null;
        }
        return str_replace("\"", " ", $string);
    }
}
